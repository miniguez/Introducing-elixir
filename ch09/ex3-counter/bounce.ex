defmodule Bounce do
  def report(count) do
    receive do
      msg -> IO.puts("Received #{count}: #{msg}")
      report(count + 1)
    end
  end
end

#Ejemplo de ejecucón
#pid3 = spawn(Bounce, :report, [1])
#send(pid3, :test)
#send(pid3, :test2)
