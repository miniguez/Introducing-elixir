defmodule Hof do
  def tripler(value, function) do
    3 * function.(value)
  end
end

#ejemplo de ejecución

# my_function = fn(value) -> 20 * value end
# Hof.tripler(20, my_function)

#ejemplo con &
# ampersand_function = &(20 * &1)
# Hof.tripler(6, ampersand_function)

#ejemplo ejecutando lamba function
# x = 20
# my_function2 = fn(value) -> x * value end
# x = 0
# my_function2.(6)

#Nota: aunque el valor de x cambie se conserva el valor en la función
