defmodule Fibo do

  def fibonacci(n) do
      fibonacci(1, n, 0)
  end

  defp fibonacci(current, n, result) when result <= n do
   s = result + current
   fibonacci(result , n, s)
   IO.puts(result)
  end

  defp fibonacci(current, _n, _result) do
    IO.puts("Finished")
    current
  end


end