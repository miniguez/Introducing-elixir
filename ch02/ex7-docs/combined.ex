defmodule Combined do
  @moduledoc """
  Función para calcular la velocidad alcanzada de un obejeto tirado al vacio.

  """
  @vsn 0.1

  import Convert
  @doc """

  Dada una altura en metros, calcular la velocidad en millas por hora
  para ese obejeto cuando se tira en la gravedad
  de la tierra,
  """
  @spec h(number()) :: number()

  def height_to_mph(meters) do
    mps_to_mph(Drop.fall_velocity(meters))
  end
end