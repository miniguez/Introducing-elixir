defmodule Drop do

  @moduledoc """
  Funciones para calcular la velocidad lograda por un objeto lanzado al vacio.
"""

  @vsn 0.1

  @doc """
  Calcula la velocidad de un obejeto callendo en la tierra
  como si estuviera en el vacio (sin resistenci de aire).
  La distancia es la altura de la cual el objeto cae,
  especificada en metros, y la función regresa
  una velocidad en metros por segundo.
"""

@spec fall_velocity(number) :: number

  def fall_velocity(distance) do
    :math.sqrt(2 * 9.8 * distance)
  end
end