defmodule Convert do
  @moduledoc """
  Funciones para convertir unidades de velocidad.
  """
  @vsn 0.1

  @doc """

  Acepta una velocidad en metros por segundo como
  argumento y regresa la velocidad equivalente
  en millas por hora
  """
  @spec mps_to_mph(number()) :: number()

  def mps_to_mph(mps) do
    2.23693629 * mps
  end

  @doc """
  Acepta una velocidad en metros por segundo
  como parametro y regresa la velocidad
  equivalente en kilometros por hora.
  """
  @spec mps_to_kph(number()) :: number()

  def mps_to_kph(mps) do
   3.6 * mps
  end
end